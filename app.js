
const dns = require('dns');
const fs = require('fs');
const request = require('request');
const args = require('minimist')(process.argv.slice(2))


const subdomains = [
    "git",
    "gitlab",
    "jira",
    "manager",
    "code",
    "stash"
]

const checkDns = (hostname) => {
    const options = {
        family: 4,
        hints: dns.ADDRCONFIG | dns.V4MAPPED,
    }
    return new Promise((resolve) => {
        dns.lookup(hostname, options, (err, address, family) => {
            if (address && !err) {
                resolve({ hostname: hostname, ip: address });
            }
        })
    })
}

const checkWebsite = (website) => {
    return new Promise((resolve) => {
        let host = 'https://' + website.hostname;
        request(host, (error, response, body) => {
            if (!error && response.statusCode == 200) {
                website.responseHeaders = response.rawHeaders;
                resolve(website);
            }
        })
    })
}


let domains = [];
if (args['site']) {
    domains.push(args['site']);
} else {
    domains = fs
        .readFileSync('./domains.txt')
        .toString('UTF8')
        .split('\n');
}

if (!domains.length) {
    throw new Error('Put domains to domains.txt file or use --site param.');
}

let generatedDomains = [];
domains.forEach(domain => {
    subdomains.forEach(subdomain => {
        generatedDomains.push(subdomain + '.' + domain);
    })
});

var dir = "./results";

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

generatedDomains.forEach(domain => {
    checkDns(domain).then(dnsInfo => {
        checkWebsite(dnsInfo).then(result => {
            if (result) {
                let resultJsonContent = JSON.stringify(result);
                fs.writeFile("./results/" + result.hostname + ".json", resultJsonContent, 'utf8', function (err) {
                    if (err) {
                        console.log("An error occured while writing JSON Object to File.");
                        return console.log(err);
                    }
                    console.log("./results/" + result.hostname + ".json JSON file has been saved.");
                });

            }
            if (args['output']) {
                console.log(result);
            }
        })
    });
})